package ee.pocos.service;


import ee.pocos.model.User;

import java.util.List;

public interface UserService {

        void save(User user);
        List<User> list();
}

