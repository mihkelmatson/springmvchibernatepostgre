package ee.pocos.model;

import org.hibernate.validator.constraints.Email;
import javax.persistence.*;
import javax.validation.constraints.Size;


@Entity
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue
    @Column(name = "UID")
    private Long id;

    @Column(name = "NAME")
    @Size(max = 20, min = 3, message = "{user.name.invalid}")
    private String name;

    @Column(name = "EMAIL", unique = true)
    @Email(message = "{user.email.invalid}")
    private String email;

    //Getter and Setter methods
    //...
}