package ee.pocos.dao;

import ee.pocos.model.User;

import java.util.List;

public interface UserDao {
    void save(User user);
    List<User> list();
}

